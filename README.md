# ASCII Game of Life

```

     ##
     ##





                                                             #
                                                             #
                                                             #
                 ##
                 ###
                #   ####
                 ## #  #
                 ######

```

Based on rules as stated in [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

## Build

`gcc -o gol gol.c`

## Run

* For an example scenario with a blinker, a block, a toad and a glider run `./gol`

* For a randomized starting scenario run `./gol -r`
